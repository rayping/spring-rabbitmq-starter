# spring-rabbitmq-starter

#### 介绍

引入rabbitmq，简化配置。

#### 安装教程

```java
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-rabbitmq-starter</artifactId>
       <version>1.0.0</version>
   </dependency>

```

1. 实现接口
   ```java
   public interface RabbitConsume<T> {
   void  onSuccess(T t);
   void  onFail(T t);
   boolean  support(Class t);
   }
   
   ```
2. 配置

  ```yml
     spring:
       rabbitmq:
         host: localhost 
         port: 5672
         username: xxx
         password: xxx
    rabbitmq:
     # 交换机
      exchanges:
       # 自定义-延迟
        - name: delay.exchange
          type: CUSTOM
          custom-type: x-delayed-message
          arguments:
            x-delayed-type: direct
     # 队列
      queues:
       # 延迟队列
        - name: delay.queue
          routing-key: delay.key
          exchange-name: delay.exchange 
  ```

#### 使用说明

1. 发送消息
   ```java
   public class DemoService {
    @Autowired
    RabbitProvider rabbitProvider;
    
    public void test() {
    rabbitProvider.send("delay.exchange","delay.key", RabbitData.builder().uuid("8888888").message(new Demo)).build(), 3000);
    ··· 代码
   }
  
   ```
2.监听   
 ```java
   public class MyRabbitConsume implements RabbitConsume<Demo> {

   @Override
   public void onSuccess(Demo o) {
      log.debug("默认处理消息成功=============================>{}" , JSONObject.toJSONString(o));
   }

   @Override
   public void onFail(Demo o) {
      log.debug("默认处理消息失败=============================>{}" , JSONObject.toJSONString(o));
   }

   @Override
   public boolean support(Class clazz) {
      return Demo.class.isAssignableFrom(clazz);
   }
 ```

### 参与贡献

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
