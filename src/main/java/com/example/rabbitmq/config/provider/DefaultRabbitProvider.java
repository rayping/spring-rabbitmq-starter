package com.example.rabbitmq.config.provider;

import com.example.rabbitmq.config.message.RabbitData;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

/**
 * @Author dengsd
 * @Date 2021/11/6 17:17
 */
@Component
@ConditionalOnMissingBean(RabbitProvider.class)
public class DefaultRabbitProvider implements RabbitProvider {
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Override
    public void send(String exchange, String routingKey, RabbitData data, int delay) {
        rabbitTemplate.convertAndSend(exchange,routingKey,data,(d)->{
            d.getMessageProperties().setDelay(delay);

            return d;
        },new CorrelationData(data.getUuid()));
    }
}
