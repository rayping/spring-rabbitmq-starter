package com.example.rabbitmq.config.message;

import lombok.*;

import java.io.Serializable;

/**
 * @Author dengsd
 * @Date 2021/11/6 15:18
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RabbitData<T> implements Serializable {
    private String uuid;
    private T message;
}
