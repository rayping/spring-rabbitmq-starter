package com.example.rabbitmq.config.comsume;

/**
 * @Author dengsd
 * @Date 2021/11/6 14:38
 */
public interface RabbitConsume<T> {
   void  onSuccess(T t);
   void  onFail(T t);
    boolean  support(Class t);
}
