package com.example.rabbitmq.config.comsume;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author dengsd
 * @Date 2021/11/6 14:49
 */
@Slf4j
public class DefaultRabbitConsume implements RabbitConsume {

    @Override
    public void onSuccess(Object o) {
        log.debug("默认处理消息成功=============================>{}" , JSONObject.toJSONString(o));
    }

    @Override
    public void onFail(Object o) {
        log.debug("默认处理消息失败=============================>{}" , JSONObject.toJSONString(o));
    }

    @Override
    public boolean support(Class clazz) {
        return Object.class.isAssignableFrom(clazz);
    }


}
