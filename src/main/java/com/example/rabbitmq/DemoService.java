package com.example.rabbitmq;

import com.example.rabbitmq.config.message.RabbitData;
import com.example.rabbitmq.config.message.RabbitDataImpl;
import com.example.rabbitmq.config.provider.RabbitProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoService {
    @Autowired
    RabbitProvider rabbitProvider;
    @GetMapping("test")
    public String test() {
        rabbitProvider.send("delay.exchange","delay.key", RabbitData.builder().uuid("8888888").message(RabbitDataImpl.builder().data("1234654").build()).build(), 1);
        return "成功";
    }
}
